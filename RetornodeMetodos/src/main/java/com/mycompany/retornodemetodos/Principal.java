/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.retornodemetodos;

/**
 *
 * @author Acer
 */
public class Principal {
    public static void main(String[] args){
        System.out.println("==>CalculadoraInt --> resultado = " + Principal.engine((int)6, (int) 5).calculate(5, 5));
        System.out.println("==>CalculadoraLong --> resultado = " + Principal.engine((long) 8, (long) 2).calculate(6, 2));
    }
    
    //Retorna un objeto de tipo "CalculadoraInt
    private static CalculadoraInt engine(int a, int b){
        return (x, y)->a*b;
    }
    
    private static CalculadoraLong engine(long a, long b){
        return (x, y)->a-b;
    }
}
