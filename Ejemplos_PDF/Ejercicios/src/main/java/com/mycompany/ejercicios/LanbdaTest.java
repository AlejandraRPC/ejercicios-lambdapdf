package com.mycompany.ejercicios;


import com.mycompany.ejercicios.FunctionTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class LanbdaTest {
    public static void main(String[] args){
        //Expresión lambda => representa un objeto de una interfaz funcional
        FunctionTest ft= () -> System.out.println("Hola Mundo");
        
        ft.saludar();
        
        LanbdaTest objeto = new LanbdaTest();
        objeto.miMetodo(ft);
    }
    
    public void miMetodo(FunctionTest parametro){
        parametro.saludar();
    }
    
}
